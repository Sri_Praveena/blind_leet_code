package com.interview.blind_leet_code;

public class BestTimeToBuyAndSellStock {

	public static void main(String[] args) {
		
		System.out.println(maxProfit(new int[] {7,1,5,3,6,4}));

	}
	
    public static int maxProfit(int[] prices) {
        
        int buy=0;
        int sell=1;
        int maxProfit=0;
        for(int i=0;i<prices.length-1;i++) {
        	if(prices[buy]<prices[sell]) {
        	int profit=prices[sell]-prices[buy];
                	maxProfit=Math.max(maxProfit, profit);
        	        	}
        	else {
    buy=sell;
  
        	}
        	sell++;
        }
        
        
        return maxProfit;
    }

}
